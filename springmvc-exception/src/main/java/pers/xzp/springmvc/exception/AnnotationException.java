package pers.xzp.springmvc.exception;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/25
 * @description:
 */
public class AnnotationException extends Exception {
    public AnnotationException(String message) {
        super(message);
    }
}
