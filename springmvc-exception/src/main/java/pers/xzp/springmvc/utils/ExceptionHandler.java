package pers.xzp.springmvc.utils;


import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import pers.xzp.springmvc.exception.CustomeException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/25
 * @// TODO:
 */
public class ExceptionHandler implements HandlerExceptionResolver {
    /**
     * 全局异常处理
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o Object
     * @param e Exception
     * @return
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        CustomeException ce = null;
        if (e instanceof CustomeException) {
            ce = (CustomeException) e;
        }else{
            ce = new CustomeException("自定义异常，测试成功");
        }
        ModelAndView modelAndView = new ModelAndView();
        System.out.println(ce.getStackTrace());
        modelAndView.addObject("error-message",ce.getMessage());
        //注意：视图，必须是配置了视图解析器，否则不能识别
        modelAndView.setViewName("error");
        return modelAndView;
    }
}
