package pers.xzp.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import pers.xzp.springmvc.exception.AnnotationException;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/25
 * @description:
 */
@Controller
@RequestMapping("/")
public class AnnotationExceptionController {

    @ExceptionHandler(AnnotationException.class)
    @RequestMapping("/toregist")
    public String toregist() throws AnnotationException {
        try {
            int i = 1 / 0;
        } catch (Exception e) {
            throw new AnnotationException("被除数不能为0");
        }
        return "register";
    }

}
