package pers.xzp.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pers.xzp.springmvc.exception.CustomeException;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/25
 * @// TODO:
 */
@Controller
@RequestMapping("/")
public class ExceptionController {

    @RequestMapping("/tologin")
    public String tologin() throws CustomeException {
        try {
            int i = 1/0;
        } catch (Exception e) {
            throw new CustomeException("1不能除0");
        }
        return "login";
    }

}
