<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath %>">
    <title>资源找不到</title>
</head>
<body>
<h2>404</h2>
<p>抱歉，您找的资源不存在或路径错误，请联系管理员！</p>
<a href="index.jsp">返回首页</a>
</body>
</html>
