<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() ;
%>
<html>
<head>
    <base href="<%=basePath %>">
    <title>文件上传</title>
    <script src="<%=basePath %>/static/js/jquery.min.js"></script>
</head>
<body>
<form action="${pageContext.request.contextPath}/upload/javaFile" method="post" enctype="multipart/form-data">
    <input type="file" name="javaFile">
    <input type="submit" value="上传">
</form>

</body>
</html>
