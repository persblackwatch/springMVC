<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
    <base href="<%=basePath %>">
    <title>首页</title>
</head>
<body>
<table border="1" cellspacing="0" >
    <%--colspan合并单元格  合并列--%>
    <th colspan="12">跳转地址</th>
    <tr>
        <td>测试</td>
        <td>练习</td>
    </tr>
    <tr>
        <td><a href="<%=basePath %>tologin">检验搭建成功与否</a></td>
        <td><a href="<%=basePath %>hw/fo">查询&添加集合元素</a></td>
    </tr>
</table>
</body>
</html>
