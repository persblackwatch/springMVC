<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%--<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>

<html>
<head>
    <base href="<%=basePath %>">
    <title>homework</title>
</head>
<body>
<table border="1" cellspacing="0" >
    <tr align="center">
        <td>序&emsp;号</td>
        <td>生&emsp;日</td>
        <td>账户名</td>
        <td>密&emsp;码</td>
    </tr>
    <c:forEach items="${userList}" var="user" varStatus="status">
        <%--
            varStatus子属性：index是指位置索引从0开始，count是计数从1开始
        --%>
        <tr align="center" >
            <td>${status.count}</td>
            <td><fmt:formatDate value="${user.birthday}" pattern="yyyy-MM-dd"/> </td>
            <td>${user.username}</td>
            <td>${user.password}</td>
        </tr>
    </c:forEach>
</table>
<br>
<form action="${basePath}hw/redirect" method="post">
    账户名&emsp;<input type="text" name="username"><br>
    密&emsp;码&emsp;<input type="text" name="password"><br>
    生&emsp;日&emsp;<input type="date" name="birthday"><br>
    <input type="submit" value="提交">
</form>
</body>
</html>
