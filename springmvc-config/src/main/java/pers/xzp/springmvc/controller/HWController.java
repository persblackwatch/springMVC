package pers.xzp.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pers.xzp.springmvc.pojo.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/23
 * @// TODO:
 */
@RequestMapping("/hw")
@Controller
public class HWController {

    List<User> userList = new ArrayList<>();

    /**
     * 初始化bean：HWController需要
     *
     * @return
     */
    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    /**
     * 初始化userList
     */
    public HWController() {
        User user = new User();
        user.setUsername("武松");
        user.setPassword("123456");
        user.setBirthday(new Date());
        userList.add(user);
        System.out.println("初始化HWController");

    }


    @RequestMapping("/fo")
    public String forward(Model model) {
        model.addAttribute("userList", userList);
        //使用forward转发，需要URL完整名，比如“/page/hw.jsp”
        return "hw";
    }

    @RequestMapping("/redirect")
    public String redirect(User user) {
        //后端接收不到user对象，导致原因是页面中的表单元素没有设置name属性（name值要与user对象属性名一致）
        if (!user.getUsername().isEmpty() || !user.getPassword().isEmpty() || user.getBirthday() != null) {
            userList.add(user);
            System.out.println(userList);
        }
        return "redirect:fo";
    }


}
