package pers.xzp.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/21
 * @// TODO:
 */
@Controller
@RequestMapping("/")
public class UserController {
    @RequestMapping("tologin")
    public String toLogin(){
        return "login";
    }

    /**
     * @see GetMapping spring4.3开始引入
     * @return String
     */
    @GetMapping("/loginOK")
    public String login(){
        return "index";
    }
}
