package pers.xzp.springmvc.pojo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/21
 * @// TODO:
 */
@Data
public class User implements Serializable {
    private int id;
    private String username;
    private String password;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    public User() {
    }
}
