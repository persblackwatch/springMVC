package pers.xzp.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pers.xzp.springmvc.pojo.User;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/25
 * @// TODO:
 */
@RequestMapping("/")
@Controller
public class ParameController {


    @GetMapping("/tologin")
    public void tologin(Model model) throws ParseException {
        User user = new User();
        user.setId(1);
        user.setUsername("张三");
        user.setPassword("123456");
        user.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse("1992-11-05"));
        model.addAttribute("oneUser",user);
    }

    /**
     * @return ModelAndView
     * @throws ParseException
     */
    @RequestMapping("/modelAndViewTest")
    public ModelAndView modelAndViewTest() throws ParseException {
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        user.setId(1);
        user.setUsername("李四");
        user.setPassword("123456");
        user.setBirthday( new SimpleDateFormat("yyyy-MM-dd").parse("1995-11-05"));
        modelAndView.addObject("modelUser",user);
        modelAndView.setViewName("parame");
        return modelAndView;
    }

    /**
     * 重定向ddf.jsp页面
     *  对于在WEB-INF目录下的资源，如何访问？
     *      只能通过controller请求，不能直接通过路径请求（包括浏览器和重定向）
     *   重定向：若是重定向到页面，需要指定路径；也可以重定向controller请求
     * @return String
     */
    @RequestMapping("/toregist")
    public String tologin(){
        return "redirect:ddf.jsp";
    }

    /**
     * @return json字符串
     * @throws ParseException
     * @throws UnsupportedEncodingException
     */
    @RequestMapping("/jsonTest")
    public @ResponseBody String jsonTest() throws ParseException, UnsupportedEncodingException {
        User user = new User();
        user.setId(5);
        user.setUsername(new String("赵武".getBytes(),"gbk"));
        user.setPassword("123456");
        user.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse("1989-11-05"));
        return user.toString();
    }
}
