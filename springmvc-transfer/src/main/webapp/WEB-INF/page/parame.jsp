<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
    <base href="<%=basePath %>">
    <title>参数传递</title>
</head>
<body>
<h2>用户信息</h2>
id ：${oneUser.id}<br>
用户名：${oneUser.username}<br>
密码：${oneUser.password}<br>
生日：<fmt:formatDate value="${oneUser.birthday}" pattern="yyyy-MM-dd"/><br>
<hr>

<h2>用户信息</h2>
id ：${modelUser.id}<br>
用户名：${modelUser.username}<br>
密码：${modelUser.password}<br>
生日：<fmt:formatDate value="${modelUser.birthday}" pattern="yyyy-MM-dd"/> <br>
</body>
</html>
